// Fill out your copyright notice in the Description page of Project Settings.

#include "aa_MainPlayer.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PawnMovementComponent.h"

#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>
 
// Sets default values
Aaa_MainPlayer::Aaa_MainPlayer()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BackCameraSpring = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
	BackCameraSpring->bUsePawnControlRotation = true;
	BackCameraSpring->SetupAttachment(RootComponent);

	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	FollowCamera->SetupAttachment(BackCameraSpring);



 

}
void Aaa_MainPlayer::StartCrouch()
{
	UE_LOG(LogTemp, Warning, TEXT("BeginPlay Crouch!"));
	//"This is a message to yourself during runtime!"
	static int i = 0;

	FString DebugMsg = FString::Printf(TEXT("Value Start %d"), i++);
	GEngine->AddOnScreenDebugMessage(2, 0.0f, FColor::Red, DebugMsg);


	UE_LOG(LogTemp, Warning, TEXT("Start  Warning Your message"));
	Crouch();
}


void Aaa_MainPlayer::StopCrouch()
{
	UE_LOG(LogTemp, Warning, TEXT("EndPlay Crouch!"));

	static int i1 = 0;

	FString DebugMsg = FString::Printf(TEXT("Value STop %d"), i1++);
	GEngine->AddOnScreenDebugMessage(2, 0.0f, FColor::Red, DebugMsg);

	UE_LOG(LogTemp, Log, TEXT("Stop Log Your message"));
 

	UnCrouch();
	
}

// Called when the game starts or when spawned
void Aaa_MainPlayer::BeginPlay()
{

	

	Super::BeginPlay();

	
}

// Called every frame
void Aaa_MainPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void Aaa_MainPlayer::CharMoveForward(float Value)
{
	AddMovementInput(GetActorForwardVector() * Value);
}
void Aaa_MainPlayer::CharMoveRight(float Value)
{
	AddMovementInput(GetActorRightVector() * Value);
}


// Called to bind functionality to input
void Aaa_MainPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);



	PlayerInputComponent->BindAxis("CharMoveForward", this, &Aaa_MainPlayer::CharMoveForward);
    PlayerInputComponent->BindAxis("CharMoveRight", this, &Aaa_MainPlayer::CharMoveRight);


	PlayerInputComponent->BindAxis("Up", this, &Aaa_MainPlayer::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn", this, &Aaa_MainPlayer::AddControllerYawInput);


	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &Aaa_MainPlayer::StartCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &Aaa_MainPlayer::StopCrouch);

	 
}

