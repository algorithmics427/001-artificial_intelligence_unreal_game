// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "aa_MainPlayer.generated.h"
class UCameraComponent;
class USpringArmComponent;
UCLASS()
class SHOOTME_API Aaa_MainPlayer : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	Aaa_MainPlayer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void CharMoveForward(float Value);
	void CharMoveRight(float Value);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		UCameraComponent* FollowCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		USpringArmComponent* BackCameraSpring;
	void StartCrouch();

	void StopCrouch();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
