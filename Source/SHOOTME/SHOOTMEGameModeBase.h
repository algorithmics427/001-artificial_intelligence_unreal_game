// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SHOOTMEGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTME_API ASHOOTMEGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
